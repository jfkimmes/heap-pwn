/**
 * Teil 1: Dieses minimale Beispiel zeigt wie ich mit einer Double Free Schwachstelle beliebige Funktionspointer überschreiben könnte um RCE zu erhalten.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int function1(int a, int b) {
    return a+b;
}

int function2(int a, int b) { // 0x40116a
    return a*b;
}

struct Foo {
    int (*some_fun)(long);
};

struct Bar {
    long long non_critical;
};

int main() {

    int *ptr1 = malloc(8);
    free(ptr1);
    ptr1[0] = 1; ptr1[1] = 1; ptr1[2] = 1; ptr1[3] = 1; ptr1[4] = 1;
    free(ptr1);

    printf("%lu\n", sizeof(struct Foo));
    printf("%lu\n", sizeof(struct Bar));

    struct Foo *foo_ptr = malloc(sizeof(struct Foo)); 
    struct Bar *bar_ptr = malloc(sizeof(struct Bar));
    //struct Foo *foo_ptr = malloc(128); 
    //struct Bar *bar_ptr = malloc(128);


    foo_ptr->some_fun = function1;

    //const char *addr = "7ffff7e04b00"; // system()
    char addr[20];
    fgets(addr,20,stdin);
    long addr_number = (long)strtol(addr, NULL, 16);
    
    bar_ptr->non_critical = addr_number;

    //const char *string = "7ffff7f6ffe5"; // /bin/sh
    char string[20];
    fgets(string,20,stdin);
    long string_number = (long)strtol(string, NULL, 16);
    printf("%i\n", foo_ptr->some_fun(string_number));

}
