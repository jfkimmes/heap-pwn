#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>

static unsigned int
_to_uint(const char *data, int data_size)
{
    unsigned int sum = 0;
    unsigned int last, i;

    last = data_size > 4 ? 3 : data_size - 1;

    for (i = 0; i <= last; i++)
        sum |= ((unsigned char) data[i]) << ((last - i) * 8);

    return sum;
}

static u_int64_t
_to_uint64(const char *data, int data_size) {
    int64_t num = 0;
    for ( int i = 0 ; i < data_size ; i++ )
        num = (num << 8) | (unsigned char) data[i];
    return num;
}

static unsigned int
_to_uint_max7b(const char *data, int data_size)
{
    unsigned int sum = 0;
    unsigned int last, i;

    last = data_size > 4 ? 3 : data_size - 1;

    for (i = 0; i <= last; i++)
        sum |= ((unsigned char) data[i]) << ((last - i) * 7);

    return sum;
}

static inline int
_is_id3v2_second_synch_byte(unsigned char byte)
{
    if (byte == 0xff)
        return 0;
    if ((byte & 0xE0) == 0xE0)
        return 1;
    return 0;
}

static long 
_find_id3v2(int fd, off_t *sync_offset)
{
    static const char pattern[3] = "ID3";
    char buffer[3];
    unsigned int prev_part_match, prev_part_match_sync = 0;
    long buffer_offset;

    if (read(fd, buffer, sizeof(buffer)) != sizeof(buffer))
        return -1;

    if (memcmp(buffer, pattern, sizeof(pattern)) == 0)
        return 0;

    return -1;
}

struct Id3v3Info {
    char track[128];
    char album[128];
    char genre[128];
    char artist[128];
    char year[128];
    struct Id3v3Helper *helper;
};

struct Id3v3Helper {
    char num[4];
    char flags[2];
    char data[10];
};

struct Id3v3Frame {
    long long offset;
    char type[8];
};

struct Id3v3MethodHandler {
    char* (*data_dup)(const char*);
    int (*callback)();
};

static long long
_helper_offset_calculation(struct Id3v3Helper *helper) {
    // ... calculates some important offset
    // depends on flags and data in helper
    return 1234;
}

static int
_some_callback() {
    return 42;
}

int 
_read_frame(int fd, struct Id3v3Frame *frame, int size, struct Id3v3Info *info) 
{
    struct Id3v3MethodHandler *handler;
    char frame_buffer[size];
    char this_string_must_be_dupd[128];

    // v3 frame offset is set to end of frame
    frame = malloc(sizeof(struct Id3v3Frame));
    frame->offset = size;

    // prepare callback handler
    handler = malloc(sizeof(struct Id3v3MethodHandler));
    handler->callback = _some_callback;
    handler->data_dup = strdup;

    if (read(fd, frame_buffer, size) != size) {
        return -1;
    }
    
    memcpy(frame->type, frame_buffer+4, 8);

    // frame-specific parsing
    if (memcmp(frame->type, "TTTTITLE", 8) == 0) {
        printf("TITLE\n");
        frame->offset = (frame->offset - _to_uint64(frame_buffer + 12, 8));
        // .. continue parsing
        // ... fill info object with title data
    } else if (memcmp(frame->type, "TTTGENRE", 8) == 0) {
        printf("GENRE\n");
        // parse ield offset from file
        frame->offset = (frame->offset - _to_uint64(frame_buffer + 12, 8));
        // .. continue parsing
        // ... fill info object with genre data
    } else if (memcmp(frame->type, "TTTTYEAR", 8) == 0) {
        printf("YEAR\n");
        frame->offset = (frame->offset - _to_uint64(frame_buffer + 12, 8));
        // .. continue parsing
        // ... fill info object with year data
    } else if (memcmp(frame->type, "TTTALBUM", 8) == 0) {
        printf("ALBUM\n");
        frame->offset = (frame->offset - _to_uint64(frame_buffer + 12, 8));
        // .. continue parsing
        // ...  fill info object with album data
    } else if (memcmp(frame->type, "INTERPRT", 8) == 0) {
        printf("INTERPRT\n");
        frame->offset = (frame->offset - _to_uint64(frame_buffer + 12, 8));
        // .. continue parsing
        // ... fill info object with interpret data
    } else {
        return -1;
    }

    // To make this more realistic, we could also use 
    // another double free vulnerability to load this 
    // string and store it in this variable.
    strcpy(this_string_must_be_dupd, frame_buffer+20);

    // duplicate a string just for fun :)
    handler->data_dup(this_string_must_be_dupd);

    free(handler);
    free(frame);
    return 0;
}


int 
_parse_id3v3(int fd, struct Id3v3Info *result) 
{
    char header_data[10];
    int frame_num;

    // first 10 (ID3+7) bytes are header data
    if (read(fd, header_data, 7) != 7) {
        return -1;
    }

    // amount of frames in tag that must be iterated over
    frame_num = _to_uint(header_data, 4);

    for (int i = 0; i<frame_num; i++) {
        struct Id3v3Helper *helper;
        struct Id3v3Frame *frame;
        int frame_size;
        char frame_header_data[10];
        long long helper_offset;

        if (read(fd, frame_header_data, 10) != 10) {
            return -1;
        }
        
        // helper get's prepared this will be doubly freed
        helper = malloc(sizeof(struct Id3v3Helper));
        memcpy(helper->flags, frame_header_data + 5, 2);

        // skip this frame if T or X flag is set.
        if (helper->flags[0] == 'T' || helper->flags[1] == 'X') {
            free(helper);
            // continue; <- programmer forgot this, leading to a double free
        }

        // Setting helper data fields 
        memcpy(helper->data, frame_header_data, 10);
        memcpy(helper->flags, frame_header_data + 5, 2);
        memcpy(helper->num, frame_header_data, 4);

        // Calculate offset helper offset from helper data
        helper_offset = _helper_offset_calculation(helper);
        free(helper);

        frame_size = _to_uint(frame_header_data, 4);
        _read_frame(fd, frame, frame_size, result);
    }
    return 0;
}

int 
main() 
{
    int fd;
    off_t sync_offset = 0;
    long id3v2_offset;
    struct Id3v3Info *info;
    info = malloc(sizeof(struct Id3v3Info));

    fd = open("./pwn.mp3", O_RDONLY);
    if (fd < 0) {
        perror("open");
        return -1;
    }

    id3v2_offset = _find_id3v2(fd, &sync_offset);

    if (id3v2_offset == 0) {
        _parse_id3v3(fd, info);
    }
    free(info);
}
