# Building the minimal PoC
- Use Ubuntu 20.04 with glibc 2.31
- `python3 ./exploit.py` (creates pwn.mp3) or `mv ./pwn.mp3.ubuntu ./pwn.mp3` (use our ubuntu pwn.mp3)
- `gcc -o vulnerable ./vulnerable.c`
- `./vulnerable` on successful exploit will execute `python ./stage2.py`

# Building Vulnerable LMS 
- setup build environment
    - install `gcc`
    - install `libtool`
    - install autotools packages 
        + e.g fedora: https://developer.fedoraproject.org/tech/languages/c/autotools.html
    + install `gettext` and `gettext-devel` (fedora)
    + install `sqlite` and `sqlite-devel`
    + install `file-devel` (we need magic.h which is provided by libmagic headers)
    + install `glib2-devel` (gio2 headers)
    + install `dbus-x11`
- run `./autogen.sh`
- `./configure CFLAGS="-g -O0"` (see below in Debugging LMS for more information)
- `make`
- run in ubuntu with glibc 2.31
- use `pwn.mp3.ubuntu` (rename to `pwn.mp3`)

## Debugging LMS
- Create a `/dbg` version:
    + `mkdir debug`
    + `cd debug`
    + ```
       ../configure --prefix=/dbg \
        CFLAGS="-g -O0"
      ```
- `make && sudo make install` will install everything under `/dbg`.
- `cp /dbg/share/dbus-1/services/org.lightmediascanner.service /usr/share/dbus-1/services/` so that `lightmediascannerctl` will find the running instance
- Run `dbus-launch` to create a private dbus daemon and take note of the resulting `DBUS_SESSION_BUS_ADDRESS`
- insert this ENV variable in the shell where you start the debugger e.g `export DBUS_SESSION_BUS_ADDRESS=unix:abstract=/tmp/dbus-atAICuPhaQ,guid=3ef3e1f3229733a2159feea9620022cb`
- do the same thing in another shell where you will start the `lightmediascannerctl` to send commands.
- in the first shell `cd /dbg/bin/` and `gdbgui`
- in `gdbgui` enter `./lightmediascannerd` as binary it should pick up the source code automatically. Run it!
    - It is important that the `lightmediascannerd` process is already running in the debugger before starting `lightmediascannerctl`. Else it will just start a new process without a debugger.
- in the other shell with the `SESSION_BUS_ADDRESS` inserted `cd /dbg/bin/` and `./lightmediascannerctl scan`


# Building AGL
- https://docs.automotivelinux.org/en/marlin/#0_Getting_Started/2_Building_AGL_Image/1_Preparing_Your_Build_Host/

## Building LMS as a component of AGL
- edit lightmediascanner recipe at `/${AGL_BUILD_DIR}/lamprey/meta-agl-demo/recipes-multimedia/lightmediascanner/lightmediascanner_0.5.1.bb`
    + change git repository to public git repo with updated LMS 
    + change the source commit
- rebuild AGL 
- mv `pwn.mp3.agl` to `/media/pwn.mp3` and  `mv stage2.py /media/stage2.py`
- or use `python3 exploit.py`
    + uncomment `#libc = ELF('./libc-2.31.so')`
    + uncomment # AGL libc ... create_frame(...)
    + change path of stage2.py payload to /media/stage2.py
- `scannerctl scan`

